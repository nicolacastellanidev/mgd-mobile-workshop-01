﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles game data and UI
/// Note: Handling UI there breaks the Single Responsibility Principle, but I've keep the UI management here only for debug purposes
/// </summary>
public class GameManager : MonoBehaviour
{
    #region Class variables
    public static GameManager Instance;

    [Tooltip("The Text to display object current count")][SerializeField] private Text objectCountText = null;
    [Tooltip("The Text to display current game speed")][SerializeField] private Text gameSpeedText = null;
    [Tooltip("The Text to record force reached")][SerializeField] private Text maxForceText = null;
    [Tooltip("The Container for cannon shooting icons")][SerializeField] private Transform cannonsFiringContainer = null;
    [Tooltip("The Cannon shooting icon prefab")][SerializeField] private GameObject cannonIconPrefab;
    private float _gameSpeed = 1f;
    private int _destroyableCount = 0;
    private float _maxForceRegistered = 0;
    private int _cannonCount = 0;
    private readonly List<Image> _cannonIconImages = new List<Image>();
    #endregion

    #region LifeCycle methods
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        foreach (Transform child in Instance.cannonsFiringContainer)
        {
            Destroy(child.gameObject); // clear cannon icons
        }
    }

    /// <summary>
    /// Sets game speed and updates UI
    /// </summary>
    private void LateUpdate()
    {
        _gameSpeed = Mathf.Clamp(_gameSpeed + Input.GetAxis("Mouse ScrollWheel"), 0.1f, 4f);
        Time.timeScale = _gameSpeed;
        Instance.gameSpeedText.text = Time.timeScale.ToString("0.00");
    }
    #endregion

    #region Class methods
    /// <summary>
    /// Hides the scene cursor and lock it
    /// </summary>
    public static void HideCursor()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    /// <summary>
    /// Increment object count and update UI text
    /// </summary>
    public static void AddDestructible()
    {
        Instance._destroyableCount++;
        Instance.objectCountText.text = Instance._destroyableCount.ToString();
    }
    
    /// <summary>
    /// Decrease object count and update UI text
    /// </summary>
    public static void RemoveDestructible()
    {
        Instance._destroyableCount--;
        Instance.objectCountText.text = Instance._destroyableCount.ToString();
    }

    /// <summary>
    /// Check if force passed is greater than current record, if it is update current record and UI
    /// </summary>
    /// <param name="force"></param>
    public static void CheckForRecordOf(float force)
    {
        if (force > Instance._maxForceRegistered)
        {
            Instance._maxForceRegistered = force;
            Instance.maxForceText.text = force.ToString(CultureInfo.InvariantCulture);
        }
    }

    /// <summary>
    /// Register dynamically a cannon for debug purposes
    /// </summary>
    /// <param name="cannon"></param>
    public static void RegisterCannon(CannonController cannon)
    {
        Instance._cannonCount++;
        var go = Instantiate(Instance.cannonIconPrefab, Vector3.zero, Quaternion.identity,
            Instance.cannonsFiringContainer);
        Instance._cannonIconImages.Add(go.GetComponent<Image>());
        cannon.OnShoot += (index) =>
        {
            if (Instance)
            {
                Instance.OnCannonShoot(index);
            }
        };
    }

    /// <summary>
    /// On cannon shoot callback
    /// </summary>
    /// <param name="index"></param>
    private void OnCannonShoot(int index)
    {
        if (!_cannonIconImages[index]) // ask to prof why sometimes we miss the reference
        {
            return;
        }

        var current = _cannonIconImages[index].color;
        current.a = 1;
        _cannonIconImages[index].color = current;
        StartCoroutine(ResetOnCannonShoot(index));
    }
    
    /// <summary>
    /// Wait a little bit, and restore cannon UI image alpha if cannon is not shooting
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    private IEnumerator ResetOnCannonShoot(int index)
    {
        yield return new WaitForSeconds(0.25f);
        if (!_cannonIconImages[index]) yield break;
        var current = _cannonIconImages[index].color;
        current.a = 0.5f;
        _cannonIconImages[index].color = current;
    }
    #endregion
}