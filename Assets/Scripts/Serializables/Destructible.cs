﻿using UnityEngine;

namespace Serializables
{

    /// <summary>
    /// Type of destructible object
    /// </summary>
    [System.Serializable]
    public enum DestructibleObjectType
    {
        Small,
        Medium,
        Big,
        SmallBlue,
        MediumBlue,
        BigBlue
    }

    /// <summary>
    /// A destructible object is composed by a type and a prefab
    /// </summary>
    [System.Serializable]
    public struct DestructibleObject
    {
        public DestructibleObjectType type;
        public GameObject prefab;
    }

}