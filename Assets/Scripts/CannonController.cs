﻿using System;
using System.Collections.Generic;
using Serializables;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Cannon controller script, handle shoots
/// </summary>
public class CannonController : MonoBehaviour
{
    #region Variables

    [Header("Cannon shoot settings")]
    [Tooltip(
        "Cannon shoot force, the final result will be a value between shootForce and shootForce/2, multiplied to the bullet mass")]
    [SerializeField]
    private float shootForce = 50f;

    [Tooltip("Vector direction for shooting")] [SerializeField]
    private Vector3 shootDirection = Vector3.zero;

    [Tooltip("Cannon fire rate (n bullet per seconds)")] [SerializeField]
    private float fireRate = 1;

    [Tooltip("Initial shoot delay (in seconds)")] [SerializeField]
    private float fireDelay = 0;

    public event Action<int> OnShoot;    // callback called on shoot

    private Vector3 _position;
    private Quaternion _rotation;
    private List<DestructibleObject> _spawnables;
    private readonly List<Vector3> _shootDirections = new List<Vector3>();

    #endregion

    #region Lifecycle methods

    private void Start()
    {
        InitData();
        InvokeRepeating(nameof(Shoot), fireDelay, fireRate);
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        var position = transform.position;
        Gizmos.DrawLine(position, position + shootDirection);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(position, position + Quaternion.AngleAxis(-45, shootDirection - position) * shootDirection);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(position, position + Quaternion.AngleAxis(45, shootDirection - position) * shootDirection);
    }
    
    #endregion

    #region Class methods

    /// <summary>
    /// Initialize spawnables data from SpawnablesData Singleton
    /// </summary>
    private void InitData()
    {
        _spawnables = SpawnablesData.GetAllSpawnables(); // get all spawnables data
        if (_spawnables.Count == 0)
        {
            Debug.LogError("Warning, you need a set of prefabs for this cannon.");
            Destroy(this);
            return;
        }

        var transf0Rm = transform;
        _position = transf0Rm.position;
        _rotation = transf0Rm.rotation;
        _shootDirections.Add(shootDirection); // default
        _shootDirections.Add(Quaternion.AngleAxis(-45, shootDirection - _position) * shootDirection); // -90 degrees shootDirection variant
        _shootDirections.Add(Quaternion.AngleAxis(45, shootDirection - _position) * shootDirection); // 90 degrees shootDirection variant
        GameManager.RegisterCannon(this); // register current to game manager
    }

    /// <summary>
    /// Shoot a random spawnable item
    /// </summary>
    private void Shoot()
    {
        var nextDestructible = _spawnables[Random.Range(0, _spawnables.Count)]; // get a random destructible
        var randomShootDirection = _shootDirections[Random.Range(0, _shootDirections.Count)]; // add randomness
        var go = Instantiate(
            nextDestructible.prefab,
            _position + (10f * randomShootDirection.normalized), // add a minimum delta to prevent wrong immediate collisions
            _rotation
        );
        go.GetComponent<Rigidbody>().AddForce(
            randomShootDirection * Random.Range(shootForce / 2, shootForce) * go.GetComponent<Rigidbody>().mass, // calc the force relative to the mass
            ForceMode.Impulse
        );
        // set actions to handle create and destroy events
        go.GetComponent<DestructibleController>().Set(
            GameManager.AddDestructible,
            GameManager.RemoveDestructible,
            0f);
        OnShoot?.Invoke(transform.GetSiblingIndex() - 2); // and invoke the shoot callback
    }

    #endregion
}