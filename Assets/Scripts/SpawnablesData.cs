﻿using System.Collections.Generic;
using Serializables;
using UnityEngine;

/// <summary>
/// Handle spawnable data, this Singleton let every component update it's data
/// according to a unique data entry point
/// </summary>
public class SpawnablesData : MonoBehaviour
{
    public static SpawnablesData Instance;
    [SerializeField] private List<DestructibleObject> spawnables = new List<DestructibleObject>();

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        } else if (Instance != this)
        {
            Destroy(this);
        }
    }
    
    /// <returns>A list of DestructibleObjects</returns>
    public static List<DestructibleObject> GetAllSpawnables()
    {
        return Instance.spawnables;
    }
    
    /// <returns>A list of DestructibleObjects filtered by it's type</returns>
    public static DestructibleObject GetSpawnableByType(DestructibleObjectType type)
    {
        return Instance.spawnables.Find(s => s.type == type);
    }
}
