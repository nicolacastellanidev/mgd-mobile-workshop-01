﻿using UnityEngine;

/// <summary>
/// Destroy objects when they exit from this collider
/// to prevent any fugitive object
/// </summary>
public class ObjectDestructor : MonoBehaviour
{
    private void OnCollisionExit(Collision collision)
    {
        // Walls can collide only with DestructibleObjects
        // Destroy objects if they exit from wall collider
        collision.gameObject.GetComponent<DestructibleController>()?.Explode(collision);
    }
}
