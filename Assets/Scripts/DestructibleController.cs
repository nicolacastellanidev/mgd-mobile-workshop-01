﻿using System;
using System.Collections;
using Serializables;
using UnityEngine;
using Utils;

/// <summary>
/// Handles a destructible object
/// </summary>
public class DestructibleController : MonoBehaviour
{
    #region Variables

    public event Action OnDestructibleCreate; // creation callback
    public event Action OnDestructibleDestroy; // destruction callback

    [Tooltip("Type of destructible object")] [SerializeField]
    private DestructibleObjectType type = DestructibleObjectType.Small;

    [Tooltip("Particles to spawn on object destroy")] [SerializeField]
    private GameObject explosionParticles = null;

    [HideInInspector] public float canDestroyAfter = 2f; // invulnerability duration

    private bool _invulnerable = true;
    private Rigidbody _rigidbody = null;
    private Coroutine _destroyCoroutine = null;

    #endregion

    #region Lifecycle methods

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        OnDestructibleCreate?.Invoke();
        Invoke(nameof(ResetInvulnerability), canDestroyAfter); // wait a little bit before enabling collisions
    }

    private void Update()
    {
        if (_rigidbody.velocity == Vector3.zero && _destroyCoroutine == null)
        {
            _destroyCoroutine = StartCoroutine(nameof(DestroyIfStopped)); // destroy after 3 seconds
        }
    }
    
    private void OnCollisionStay(Collision collision)
    {
        if (_invulnerable)
        {
            return;
        }

        _invulnerable = true;
        GetComponent<BoxCollider>().enabled = false;
        Explode(collision);
    }

    #endregion

    #region Class methods

    /// <summary>
    /// Set the destructible data
    /// </summary>
    /// <param name="onDestructibleCreateAction"></param>
    /// <param name="onDestructibleDestroyAction">Destroy callback</param>
    /// <param name="destroyableAfter">Invulnerability time</param>
    public void Set(Action onDestructibleCreateAction, Action onDestructibleDestroyAction,
        float destroyableAfter)
    {
        OnDestructibleCreate += onDestructibleCreateAction;
        OnDestructibleDestroy += onDestructibleDestroyAction;
        canDestroyAfter = destroyableAfter;
    }

    /// <summary>
    /// Make object explode, if object is NOT small then spawn little objects
    /// </summary>
    /// <param name="collision">The collision between 2 objects</param>
    public void Explode(Collision collision)
    {
        switch (type)
        {
            case DestructibleObjectType.Big: // spawn medium objects
                SpawnChilds(SpawnablesData.GetSpawnableByType(DestructibleObjectType.Medium), collision);
                break;
            case DestructibleObjectType.BigBlue: // spawn medium objects
                SpawnChilds(SpawnablesData.GetSpawnableByType(DestructibleObjectType.MediumBlue), collision);
                break;
            case DestructibleObjectType.Medium: // spawn small objects
                SpawnChilds(SpawnablesData.GetSpawnableByType(DestructibleObjectType.Small), collision);
                break;
            case DestructibleObjectType.MediumBlue: // spawn small objects
                SpawnChilds(SpawnablesData.GetSpawnableByType(DestructibleObjectType.SmallBlue), collision);
                break;
        }
        OnDestructibleDestroy?.Invoke();
        DestroyAndMakeParticles();
    }

    /// <summary>
    /// Destroy current gameObject and spawn destroy particles
    /// </summary>
    private void DestroyAndMakeParticles()
    {
        if (explosionParticles)
        {
            var pGo = Instantiate(explosionParticles, transform.position, transform.rotation);
            pGo.transform.localScale = transform.localScale / 2f; // big brain time
        }

        Destroy(gameObject);
    }

    /// <summary>
    /// If current gameObject is not moving, destroy it
    /// </summary>
    private IEnumerator DestroyIfStopped()
    {
        yield return new WaitForSeconds(3);
        if (_rigidbody.velocity == Vector3.zero)
        {
            DestroyAndMakeParticles();
        }

        _destroyCoroutine = null;
    }

    private void ResetInvulnerability()
    {
        _invulnerable = false;
    }

    private void SpawnChilds(DestructibleObject nextPrefab, Collision collision)
    {
        var rotation = transform.rotation;
        var position = transform.position;
        for (int i = 0; i < collision.contacts.Length; i++)
        {
            var go = Instantiate(nextPrefab.prefab, position, rotation);
            go.GetComponent<Rigidbody>().AddForce(
                collision.GetForce(position, i),
                ForceMode.Impulse
            );
        }
    }

    #endregion
}