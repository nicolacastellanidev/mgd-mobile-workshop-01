﻿# Mobile First Excercise

##### MGD - 2020/2021 - NICOLA CASTELLANI - VR466107

## GOAL

Design and implement the following system.

1. Create the following prefabs, evaluating whether or not to use variants:
    * Large object with a mass of 10.
    * Medium-size object with a mass of 5.
    * Small object with a mass of 1.
2. Create a scene with a room (approx 10 times larger than the large object).
3. When the scene starts, periodically shot physically-simulated objects in random directions from a couple of fixed
   points in the room.
4. When an object collides with the room or another object, destroy it and create an appropriate number of smaller
   objects, preserving momentum. Newly created objects should not be destroyed immediately when colliding with “sibling”
   objects or the ones that caused the original collision.
5. Destroy objects that stay still for a certain amount of time (ie. 3 seconds).

## PROJECT SETUP

* ***Fonts*** - Project UI fonts
* ***Materials*** - Project materials
* ***Prefabs*** - A list of prefabs used in this project, contains **Box** prefabs and variants (*Blue boxes*)
* ***Scenes*** - Contains a couple of scenes used in this project
    + **Prototyping** - project main scene
    + **PrototypingButCool** - secondary scene, same as the main one, but with cool things
    + **Particles** - scene for particles testing
* ***Scripts*** - A set of scripts for project
    - **Serializables/** - serializable data
    - **Utils/** - set of utils for collision detection
* ***Sounds*** - sounds used in project
* ***Sprites*** - set of sprites made with [Aseprite](https://www.aseprite.org/)

## GETTING STARTED

Go to ***Scenes/Prototype*** and press Play. You can move camera around using these commands:
1. **WASD** to move camera on X and Z axis
2. **QE** to move camera on Y axis
3. **MouseWheel** to change time scale

## MOMENTUM KEEPING

I've started the project with a simple momentum-keeping system, then I've moved to a complex one using the 
[Coefficent Of Restitution](https://en.wikipedia.org/wiki/Coefficient_of_restitution) formula, which gives
collisions a more realistic keep of momentum on spawned objects.

The initial idea was to spawn N children and give them an inverse velocity according to last parent rigidbody velocity.
![Image](./000.png)

As you can see after a collision, N child objects are generated. They're velocity is the same as the original parent,
but reversed.

The second method calculate force and direction for each collision point:

![Image](./001.png)

A child object is created for each collision point, and for each of them a new Vector3 is calculate for the new velocity.
This vector is created multiplying the *collision force* to the *collision direction*:

```
The force is calculated from this formula:

    F = m • a 

or 

    F = m • ∆v / t

and the impulse is calculated by:

    impulse = F • t = m • ∆v

so

    F (collision force) = m • ∆v / t = impulse / t
```

and the direction is calculated substracting the target position to the collision point.

This gives a more realistic momentum to spawned childs. Read more on ***CollisionExtension*** file.

## RANDOM SHOTTING

I've decided to create 12 cannons, every cannon can shoot in a specific cone defined by the default direction
and 2 other dinamically generated around 90 degrees:

![Image](./002.png)

so the cannon shoots from a random direction between these 3 vectors.

## NOTES ABOUT DEVELOPMENT

1. I've made 3 prefabs and 3 variant for size and color (pink and blue)
2. Big prefabs spawn Medium prefabs, and Medium prefabs spawns small prefabs, smalls just explode
3. I've clamped collision force to a maximum of 50 to prevent collision tunneling and to give a reasonable speed to
   objects.
4. GameManager is also handling some portions of UI, this is for debug purposes
5. I've started with Unity 2020, which has a different physics system, then I downgraded to 2019 according to homework.
I've noticed small differences about physics especially on force calculations (2019 is less precise).